import React from "react";
import { View } from "react-native";
import { createAppContainer } from "react-navigation";
import { useScreens } from "react-native-screens";
import AppNavigator from "./screens";

// https://reactnavigation.org/docs/en/react-native-screens.html
useScreens();

const AppContainer = createAppContainer(AppNavigator);

// https://reactnavigation.org/docs/en/common-mistakes.html#wrapping-appcontainer-in-a-view-without-flex
const appStyles = { flex: 1 };

const App = () => (
  <View style={appStyles}>
    <AppContainer />
  </View>
);

export default App;
