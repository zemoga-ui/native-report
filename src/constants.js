export const TOKEN = "@token";
export const NAME = "@name";
export const EMAIL = "@email";
export const BASECAMP_ID = "@basecampId";
export const SESSION = [TOKEN, NAME, EMAIL, BASECAMP_ID];
export const SCREEN_NAMES = {
  reports: "Reports",
  team: "Team",
  profile: "Profile",
  projects: "Projects",
  todos: "Todos",
  authLoading: "AuthLoading",
  login: "Login",
  timesheet: "Timesheet"
};
