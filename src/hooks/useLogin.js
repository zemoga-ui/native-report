import { useState, useCallback } from "react";
import { CLIENT_ID, CLIENT_SECRET } from "react-native-dotenv";
import { authorize } from "react-native-app-auth";
import AsyncStorage from "@react-native-community/async-storage";
import { TOKEN, EMAIL, NAME, BASECAMP_ID, SCREEN_NAMES } from "../constants";
import callApi from "../utils/callApi";

const config = {
  clientId: CLIENT_ID,
  clientSecret: CLIENT_SECRET,
  redirectUrl: "com.zemoga:/callback",
  scopes: [],
  serviceConfiguration: {
    authorizationEndpoint: "https://launchpad.37signals.com/authorization/new",
    tokenEndpoint: `https://launchpad.37signals.com/authorization/token?type=web_server&client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}`
  },
  additionalParameters: {
    type: "web_server"
  }
};

const useLogin = navigation => {
  const [result, setResult] = useState("");
  const [error, setError] = useState(null);

  const login = useCallback(async () => {
    // use the client to make the auth request and receive the authState
    try {
      const basecampAuth = await authorize(config);

      await AsyncStorage.setItem(TOKEN, basecampAuth.accessToken);

      const jsonInfo = await callApi(
        "https://launchpad.37signals.com/authorization.json"
      );

      const {
        identity: { id, email_address, first_name, last_name } = {}
      } = jsonInfo;

      await Promise.all([
        AsyncStorage.setItem(EMAIL, email_address),
        AsyncStorage.setItem(NAME, `${first_name} ${last_name}`),
        AsyncStorage.setItem(BASECAMP_ID, id)
      ]);

      setResult(jsonInfo);

      if (navigation) {
        navigation.navigate(SCREEN_NAMES.timesheet);
      }
    } catch (err) {
      setError(err);
    }
  }, []);

  return {
    login,
    error,
    result
  };
};

export default useLogin;
