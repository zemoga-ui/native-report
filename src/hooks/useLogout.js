import { useState, useCallback } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import { SESSION, SCREEN_NAMES } from "../constants";

const useLogout = navigation => {
  const [error, setError] = useState(null);

  const logout = useCallback(async () => {
    try {
      await AsyncStorage.multiRemove(SESSION);

      if (navigation) {
        navigation.navigate(SCREEN_NAMES.login);
      }
    } catch (err) {
      setError(err);
    }
  }, []);

  return {
    error,
    logout
  };
};

export default useLogout;
