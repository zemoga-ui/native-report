import React from "react";
import { Button, Text } from "react-native";
import { SafeAreaView } from "react-navigation";
import { SCREEN_NAMES } from "../constants";

const styles = { flex: 1, alignItems: "center", justifyContent: "center" };

const TodosScreen = ({ navigation }) => (
  <SafeAreaView style={styles}>
    <Text>Todos Screen</Text>
    <Button
      title="Go to Reports?"
      onPress={() => navigation.navigate(SCREEN_NAMES.reports)}
    />
  </SafeAreaView>
);

export default TodosScreen;
