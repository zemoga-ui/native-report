import React from "react";
import { Button, Text } from "react-native";
import { SafeAreaView } from "react-navigation";
import { SCREEN_NAMES } from "../constants";

const styles = { flex: 1, alignItems: "center", justifyContent: "center" };

const ProjectsScreen = ({ navigation }) => (
  <SafeAreaView style={styles}>
    <Text>Projects Screen</Text>
    <Button
      title="Navigate to Todos"
      onPress={() => navigation.navigate(SCREEN_NAMES.todos)}
    />
  </SafeAreaView>
);

export default ProjectsScreen;
