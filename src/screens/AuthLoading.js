import React, { useEffect } from "react";
import { Text } from "react-native";
import { SafeAreaView } from "react-navigation";
import AsyncStorage from "@react-native-community/async-storage";
import { TOKEN, SCREEN_NAMES } from "../constants";

const styles = { flex: 1, alignItems: "center", justifyContent: "center" };

const AuthLoadingScreen = ({ navigation }) => {
  useEffect(() => {
    const checkAuth = async () => {
      const accessToken = await AsyncStorage.getItem(TOKEN);

      // TODO: validate stored accessToken
      navigation.navigate(
        accessToken ? SCREEN_NAMES.timesheet : SCREEN_NAMES.login
      );
    };

    checkAuth();
  }, []);

  return (
    <SafeAreaView style={styles}>
      {/* TODO: remove and replace this with a component that gets directly mounted below */}
      <Text>Hi, I'm loading 🌝</Text>
    </SafeAreaView>
  );
};

export default AuthLoadingScreen;
