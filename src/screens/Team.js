import React from "react";
import { Text } from "react-native";
import { SafeAreaView } from "react-navigation";

const styles = { flex: 1, alignItems: "center", justifyContent: "center" };

const TeamScreen = () => (
  <SafeAreaView style={styles}>
    <Text>Team Screen</Text>
  </SafeAreaView>
);

export default TeamScreen;
