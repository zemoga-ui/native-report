import React from "react";
import { Button, Text } from "react-native";
import { SafeAreaView } from "react-navigation";
import useLogin from "../hooks/useLogin";

const styles = {
  background: "rgb(250, 250, 250)",
  flex: 1,
  alignItems: "center",
  justifyContent: "center"
};

const LoginScreen = ({ navigation }) => {
  const { login, result } = useLogin(navigation);

  return (
    <SafeAreaView style={styles}>
      <Text style={styles.data}>{JSON.stringify(result)}</Text>
      <Button onPress={login} title="Login with Basecamp" />
    </SafeAreaView>
  );
};

export default LoginScreen;
