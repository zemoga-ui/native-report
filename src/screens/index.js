import {
  createSwitchNavigator,
  createStackNavigator,
  createBottomTabNavigator
} from "react-navigation";
import AuthLoadingScreen from "./AuthLoading";
import LoginScreen from "./Login";
import ProjectsScreen from "./Projects";
import ReportsScreen from "./Reports";
import TeamScreen from "./Team";
import TodosScreen from "./Todos";
import { SCREEN_NAMES } from "../constants";

const ProfileTabs = createBottomTabNavigator(
  {
    [SCREEN_NAMES.reports]: ReportsScreen,
    [SCREEN_NAMES.team]: TeamScreen
  },
  {
    initialRouteName: SCREEN_NAMES.reports
  }
);

const TimesheetStack = createStackNavigator(
  {
    [SCREEN_NAMES.profile]: ProfileTabs,
    [SCREEN_NAMES.projects]: ProjectsScreen,
    [SCREEN_NAMES.todos]: TodosScreen
  },
  {
    initialRouteName: SCREEN_NAMES.profile,
    defaultNavigationOptions: {
      title: "z-timesheet",
      headerStyle: {
        backgroundColor: "rgb(254, 254, 254)"
      },
      headerTintColor: "rgb(212, 212, 212)",
      headerTitleStyle: {
        fontWeight: "bold",
        color: "rgb(50, 195, 120)"
      }
    }
  }
);

const AppNavigator = createSwitchNavigator(
  {
    [SCREEN_NAMES.authLoading]: AuthLoadingScreen,
    [SCREEN_NAMES.login]: LoginScreen,
    [SCREEN_NAMES.timesheet]: TimesheetStack
  },
  {
    initialRouteName: SCREEN_NAMES.authLoading
  }
);

export default AppNavigator;
