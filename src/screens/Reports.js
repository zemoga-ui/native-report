import React, { useState, useEffect } from "react";
import { Button, Text } from "react-native";
import { SafeAreaView } from "react-navigation";
import AsyncStorage from "@react-native-community/async-storage";
import useLogout from "../hooks/useLogout";
import { NAME, SCREEN_NAMES } from "../constants";

const styles = { flex: 1, alignItems: "center", justifyContent: "center" };

const ReportsScreen = ({ navigation }) => {
  const { logout } = useLogout(navigation);
  const [name, setName] = useState("No Name 👤");

  useEffect(() => {
    const getName = async () => {
      const sessionName = await AsyncStorage.getItem(NAME);

      setName(sessionName);
    };

    getName();
  }, []);

  return (
    <SafeAreaView style={styles}>
      <Text>Reports Screen</Text>
      <Text>Hello there, {name} 👋🏻</Text>
      <Button
        title="Navigate to Projects"
        onPress={() => navigation.navigate(SCREEN_NAMES.projects)}
      />
      <Button title="Temp Logout 🌝" onPress={logout} />
    </SafeAreaView>
  );
};

export default ReportsScreen;
