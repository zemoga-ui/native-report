import AsyncStorage from "@react-native-community/async-storage";
import { parseString } from "react-native-xml2js";
import { TOKEN } from "../constants";

const parseXml = async response => {
  const textResponse = await response.text();

  return new Promise((resolve, reject) => {
    parseString(textResponse, (err, result) => {
      if (err) {
        reject(err);
      }

      resolve(result);
    });
  });
};

const callApi = async (url, doParseXml, opts = {}) => {
  const accessToken = await AsyncStorage.getItem(TOKEN);

  const response = await fetch(url, {
    ...opts,
    headers: {
      Authorization: `Bearer ${accessToken}`,
      ...opts.headers
    }
  });

  return doParseXml ? parseXml(response) : response.json();
};

export default callApi;
